Rails.application.routes.draw do

  namespace :admin do
      resources :answers
      resources :questions
      resources :tenants
      resources :users

      root to: "answers#index"
    end
 root 'welcome#index'

end

Rails.application.routes.draw do
  namespace :admin do
      resources :answers
      resources :questions
      resources :tenants
      resources :users

      root to: "answers#index"
    end
	namespace 'api' do
		namespace 'v1' do
			resources :questions
			resources :answers
			resources :users
			resources :tenants
		end
	end
end
