module Api
	module V1
		class TenantsController < ApplicationController
			def index
				tenants = Tenant.order('created_at DESC');
				render json: {status: 'SUCCESS', message: 'Loaded tenants', data:tenants},status: :ok
			end

			def show
				tenant = Tenant.find (params[:id])
				render json: {status: 'SUCCESS', message: 'Loaded Tenant', data:tenant},status: :ok
			end

			def create
				tenant = Tenant.new (tenant_params)

				if tenant.save
					render json: {status: 'SUCCESS', message: 'Saved tenant', data:tenant},status: :ok

				else
					render json: {status: 'ERROR', message: 'Tenant not saved', data:tenant.errors},status: :unprocessable_entity
				end
			end

			def destroy
				tenant = Tenant.find (params[:id])
				tenant.destroy
				render json: {status: 'SUCCESS', message: 'Deleted tenant', data:tenant},status: :ok
			end

			def update
				tenant = Tenant.find (params[:id])
				if tenant.update_attributes(tenant_params)
					render json: {status: 'SUCCESS', message: 'Updated tenant', data:tenant},status: :ok

				else
					render json: {status: 'ERROR', message: 'Tenant not Updated', data:tenant.errors},status: :unprocessable_entity
				end
			end

			private

			def tenant_params
				params.permit(:name, :api_key)
			end

		end
	end
end