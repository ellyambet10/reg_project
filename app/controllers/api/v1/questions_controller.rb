module Api
	module V1
		class QuestionsController < ApplicationController
			def index
				questions = Question.order('created_at DESC');
				render json: {status: 'SUCCESS', message: 'Loaded questions', data:questions},status: :ok
			end

			def show
				question = Question.find (params[:id])
				render json: {status: 'SUCCESS', message: 'Loaded question', data:question},status: :ok
			end

			def create
				question = Question.new (question_params)

				if question.save
					render json: {status: 'SUCCESS', message: 'Saved question', data:question},status: :ok

				else
					render json: {status: 'ERROR', message: 'Question not saved', data:question.errors},status: :unprocessable_entity
				end
			end

			def destroy
				question = Question.find (params[:id])
				question.destroy
				render json: {status: 'SUCCESS', message: 'Deleted question', data:question},status: :ok
			end

			def update
				question = Question.find (params[:id])
				if question.update_attributes(question_params)
					render json: {status: 'SUCCESS', message: 'Updated question', data:question},status: :ok

				else
					render json: {status: 'ERROR', message: 'Question not Updated', data:question.errors},status: :unprocessable_entity
				end
			end

			private

			def question_params
				params.permit(:title, :private, :user_id)
			end

		end
	end
end